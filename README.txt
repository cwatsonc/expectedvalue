This is a simplified real world example of an operation at the heart 
of WiserCare’s platform.  It is built with gradle (build), spring boot 
(platform), java 8(language) junit(test) and lombok (reducing boilerplate).

There is a test in the project that explains the setup (act one), 
execution (act two) and verification (act three). The execution 
part is not implemented currently. That is what you need to do :)

There are no tricks, traps, or gotchas. There is no one perfect ideal 
way to implement this that we are looking for. We just need to 
understand how you approach a problem. 

You can solve this in whatever way feels natural to you. If you want 
to make any additional classes or methods or bring in any additional 
packages, go for it.

package com.wisercare.expectedvalue;

import lombok.Data;

@Data
public class TreatmentExpectedValue
{
    private Treatment treatment;

    //this is a type of "score" that indicates how well the treatment aligns with what the patient cares about
    private Double expectedValue;

    public TreatmentExpectedValue(Treatment treatment, Double expectedValue) {
        this.treatment = treatment;
        this.expectedValue = expectedValue;
    }
}

package com.wisercare.expectedvalue;


import lombok.Data;

@Data
//an outcome is a possible result of treatment, it could be a negative side effect or a positive benefit
//outcomes are related to treatments that cause them through probabilities
public class Outcome {
    private String name;
    private String description;

    public Outcome(String name, String description) {
        this.name = name;
        this.description = description;
    }
}

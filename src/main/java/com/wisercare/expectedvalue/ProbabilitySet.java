package com.wisercare.expectedvalue;

import java.util.HashMap;
import java.util.Map;

public class ProbabilitySet {

    private Map<ProbabilityKey, Probability> probabilities = new HashMap<>();
    public void addProbability(Treatment treatment, Outcome outcome, Double percentChance) {
        Probability probability = new Probability(treatment, outcome, percentChance);
        ProbabilityKey key = new ProbabilityKey(treatment, outcome);
        probabilities.put(key, probability);
    }

    public Double getPercentChance(Treatment treatment, Outcome outcome) {
        ProbabilityKey key = new ProbabilityKey(treatment, outcome);
        if (probabilities.containsKey(key)) {
            return probabilities.get(key).getPercentChance();
        }
        return null;
    }
}
